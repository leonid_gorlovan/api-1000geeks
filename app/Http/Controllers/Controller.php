<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function responseSuccess($data = null)
    {
        return response()->json([
            'status' => 1, 
            'data' => $data
        ]);
    }

    public function responseFail($data = null)
    {
        return response()->json([
            'status' => 0, 
            'error' => $data
        ]);
    }
}
