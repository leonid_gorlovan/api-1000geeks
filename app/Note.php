<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = [
        'title', 'description', 'api_token'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'api_token',
    ];
}
